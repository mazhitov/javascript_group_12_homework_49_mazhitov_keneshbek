const size = 8;
const whiteSquare = '  ';
const blackSquare = '██';
let line = '';
for (let rowIndex = 1; rowIndex <= size; rowIndex++) {
    for (let lineIndex = 1; lineIndex <= size; lineIndex++) {
        if ((rowIndex % 2) === (lineIndex % 2)) {
            line+=whiteSquare;
        } else {
            line+=blackSquare;
        }
    }
    line+='\r\n';
}
console.log(line);

